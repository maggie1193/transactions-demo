package com.zooplus.transactionsdemo.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "PERSONS")
public class Person {

    @Id
    @SequenceGenerator(name = "PERSONS_SEQUENCE", sequenceName = "PERSONS_SEQ")
    @GeneratedValue(generator = "PERSONS_SEQUENCE")
    @Column(name = "P_ID")
    private Long id;

    @Column(name = "P_NAME")
    private String name;

    @Column(name = "P_SEC_NAME")
    private String setSecNme;
}

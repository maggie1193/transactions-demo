package com.zooplus.transactionsdemo.repository;

import com.zooplus.transactionsdemo.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


//@Transactional(propagation = Propagation.MANDATORY)
public interface PersonRepository extends JpaRepository<Person, Long> {

    Person findByName(String name);
}

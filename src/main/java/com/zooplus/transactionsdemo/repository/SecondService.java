package com.zooplus.transactionsdemo.repository;

import com.zooplus.transactionsdemo.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SecondService {

    @Autowired
    private PersonRepository personRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void doSTh() {
        personRepository.findByName("hhhd");
        Person person = Person.builder().name("Pers2222").build();
        Person p = personRepository.save(person);
        System.out.print("a");
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void doSThElse(Person person) {
        person.setSetSecNme("sdadad");
        System.out.print("a");
    }

    //@Transactional(propagation = Propagation.REQUIRES_NEW)
    public void doSThElse2() {
        Person person = personRepository.findByName("Pers");
        person.setSetSecNme("ads");

    }
}

package com.zooplus.transactionsdemo.runner;

import com.zooplus.transactionsdemo.PersonService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class AppRunner implements CommandLineRunner {

    private final PersonService personService;

    public AppRunner(PersonService personService) {
        this.personService = personService;
    }

    @Override
    public void run(String... args) throws Exception {
        personService.createPerson();
    }
}

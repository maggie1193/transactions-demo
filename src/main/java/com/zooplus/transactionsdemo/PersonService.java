package com.zooplus.transactionsdemo;

import com.zooplus.transactionsdemo.domain.Person;
import com.zooplus.transactionsdemo.repository.PersonRepository;
import com.zooplus.transactionsdemo.repository.SecondService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class PersonService {

    private final PersonRepository personRepository;

    @Autowired
    private SecondService secondService;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }


    @Transactional
    public void createPerson() {
        //Optional<Person> p = personRepository.findById(1L);

        Person person = Person.builder().name("Pers").build();
        Person p = personRepository.save(person);
    //    p.setName("jdksk");
//        secondService.doSTh();
      //  secondService.doSThElse(p);
        secondService.doSThElse2();
    }
}
